#!/usr/bin/env python

import json
import sys

from haralyzer import HarParser, HarPage

with open(sys.argv[1]) as f:
    har_parser = HarParser(json.loads(f.read()))

    data = har_parser.har_data

    urls = set()

    for entry in data['entries']:
        url = entry['request']['url']
        urls.add(url)

    for url in urls:
        print(url)
